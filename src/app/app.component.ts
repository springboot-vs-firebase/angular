import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private primengConfig: PrimeNGConfig, private router: Router) {}

  public ngOnInit() {
    this.primengConfig.ripple = true;
  }

  public navigateToSpringboot(): void {
    this.router.navigate(['springboot']);
  }

  public navigateToFirebase(): void {
    this.router.navigate(['firebase']);
  }
}
