import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Auth,
  createUserWithEmailAndPassword,
  getAuth,
  GoogleAuthProvider,
  OAuthCredentialOptions,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signInWithPopup,
  User,
  UserCredential,
} from 'firebase/auth';
import { MessageService } from 'primeng/api';
import { FirebaseError } from 'firebase/app';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private auth: Auth;
  private googleAuth: GoogleAuthProvider;
  private currentUser!: User;
  public authStateChanged = new Subject<User | null>();

  constructor(private messageService: MessageService) {
    this.googleAuth = new GoogleAuthProvider();
    this.auth = getAuth();
    this.setLanguage();
    this.listenForAuthStateChanged();
  }

  public googleSignin(): Promise<UserCredential> {
    return signInWithPopup(this.auth, this.googleAuth);
  }

  public emailSignup(email: string, password: string): Promise<UserCredential> {
    return createUserWithEmailAndPassword(this.auth, email, password);
  }

  public emailSignin(email: string, password: string): Promise<UserCredential> {
    return signInWithEmailAndPassword(this.auth, email, password);
  }

  public listenForAuthStateChanged(): void {
    onAuthStateChanged(this.auth, (user) => {
      this.currentUser = user as User;
      this.authStateChanged.next(user);
    });
  }

  public logout(): void {
    this.auth.signOut();
  }

  public getCredentialFromResult(
    result: UserCredential
  ): OAuthCredentialOptions {
    return GoogleAuthProvider.credentialFromResult(
      result
    ) as OAuthCredentialOptions;
  }

  public handleGoogleAuthError(error: FirebaseError): void {
    const credential = GoogleAuthProvider.credentialFromError(error);
    this.messageService.add({
      severity: 'error',
      summary: 'Fehler beim Einloggen mit Google',
      detail: error.message,
      life: 3000,
    });
    console.error(error, credential);
  }

  public handleEmailAuthError(error: FirebaseError): void {
    const credential = GoogleAuthProvider.credentialFromError(error);
    this.messageService.add({
      severity: 'error',
      summary: 'Fehler beim Einloggen per E-Mail',
      detail: error.message,
      life: 3000,
    });
    console.error(error, credential);
  }

  public getCurrentUser(): User {
    return this.currentUser;
  }

  public getCurrentUserId(): string {
    return this.getCurrentUser().uid;
  }

  private setLanguage(languageCode = 'de'): void {
    this.auth.languageCode = languageCode;
  }
}
