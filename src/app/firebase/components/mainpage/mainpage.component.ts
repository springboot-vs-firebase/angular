import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User, UserCredential } from 'firebase/auth';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.scss'],
})
export class MainpageComponent implements OnInit {
  public isLogin = false;
  public username = '';

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  public ngOnInit() {
    this.userActivityChanged(this.authenticationService.getCurrentUser());
    this.listenForUserLoginActivity();
  }

  public loginWithGoogle(): void {
    this.authenticationService
      .googleSignin()
      .then((user: UserCredential) => {
        this.isLogin = true;
        console.log(user);
      })
      .catch((error) =>
        this.authenticationService.handleGoogleAuthError(error)
      );
  }

  public logout(): void {
    this.authenticationService.logout();
  }

  public navigateToEmailLogin(): void {
    this.router.navigate(['email-login']);
  }

  private listenForUserLoginActivity(): void {
    this.authenticationService.authStateChanged.subscribe((user) => {
      this.userActivityChanged(user as User);
    });
  }

  private userActivityChanged(user: User): void {
    if (user) {
      this.isLogin = true;
      if (user.displayName) {
        this.username = user.displayName;
      } else if (user.email) {
        this.username = user.email;
      }
      console.log(user);
    } else {
      this.isLogin = false;
      console.log('Logout');
    }
  }
}
