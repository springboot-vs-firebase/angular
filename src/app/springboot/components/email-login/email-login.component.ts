import { MessageService } from 'primeng/api';
import {
  AuthenticationService,
  User,
} from './../../services/authentication.service';
import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-email-loginsb',
  templateUrl: './email-login.component.html',
  styleUrls: ['./email-login.component.scss'],
})
export class EmailLoginSBComponent {
  @Output() private loginSucceed = new EventEmitter();

  public email = '';
  public password = '';

  constructor(
    private authenticationService: AuthenticationService,
    private messageService: MessageService
  ) {}

  public login(): void {
    this.authenticationService.getAll().subscribe((users) => {
      let passwordFound = false;
      users.forEach((user) => {
        if (user.email === this.email && user.password === this.password) {
          this.loginSucceed.emit();
          passwordFound = true;
        }
      });

      if (!passwordFound) {
        this.messageService.add({
          severity: 'error',
          summary: 'Fehler',
          detail: 'Falsche Logindaten',
          life: 3000,
        });
      }
    });
  }

  public register(): void {
    const user: User = {
      email: this.email,
      password: this.password,
    };
    this.authenticationService.create(user).subscribe((users) => {
      this.loginSucceed.emit();
    });
  }
}
