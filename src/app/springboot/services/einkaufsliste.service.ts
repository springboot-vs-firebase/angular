import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class EinkaufslisteService {
  private readonly url = 'http://localhost:8080/eintrag';

  constructor(private httpClient: HttpClient) {}

  public getAll(): Observable<Listeneintrag[]> {
    return this.httpClient.get<Listeneintrag[]>(this.url);
  }

  public create(eintrag: Listeneintrag): Observable<Listeneintrag> {
    return this.httpClient.post<Listeneintrag>(this.url, eintrag);
  }

  public update(eintrag: Listeneintrag): Observable<Listeneintrag> {
    return this.httpClient.post<Listeneintrag>(
      `${this.url}/${eintrag.id}`,
      eintrag
    );
  }

  public delete(id: number): Observable<void> {
    return this.httpClient.delete<void>(`${this.url}/${id}`);
  }
}

export interface Listeneintrag {
  id: number;
  artikel: string;
  anzahl: number;
  gekauft: boolean;
}
