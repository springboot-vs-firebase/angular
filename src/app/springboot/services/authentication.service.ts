import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private readonly url = 'http://localhost:8080/user';

  constructor(private httpClient: HttpClient) {}

  public getAll(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.url);
  }

  public create(eintrag: User): Observable<User> {
    return this.httpClient.post<User>(this.url, eintrag);
  }
}

export interface User {
  id?: number;
  email: string;
  password: string;
}
