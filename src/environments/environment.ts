// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'angular-vs-firebase',
    appId: '1:426986743403:web:831e211cf5b172f044b32e',
    storageBucket: 'angular-vs-firebase.appspot.com',
    apiKey: 'AIzaSyDrAOr06XT0SrzKYsIhOgc71Zm4eRlSMbU',
    authDomain: 'angular-vs-firebase.firebaseapp.com',
    messagingSenderId: '426986743403',
    measurementId: 'G-6W6DPBWGJZ',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
